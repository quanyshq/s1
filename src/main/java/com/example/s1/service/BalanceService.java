package com.example.s1.service;

import com.example.s1.exception.InterruptedRuntimeException;
import com.example.s1.model.Transfer;
import com.example.s1.repository.TransferRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
@Slf4j
public class BalanceService {

	private final TransferRepository transferRepository;

	@Transactional
	public void warmup(Long accountId, Long periodId) {
		calculateBalance(accountId, periodId);
	}

	@Transactional(isolation = Isolation.READ_UNCOMMITTED)
	public void calculateBalanceOnReadUncommittedIsolationLevel(Long accountId, Long periodId) {
		calculateBalance(accountId, periodId);
	}

	@Transactional(isolation = Isolation.READ_COMMITTED)
	public void calculateBalanceOnReadCommittedIsolationLevel(Long accountId, Long periodId) {
		calculateBalance(accountId, periodId);
	}

	@Transactional(isolation = Isolation.REPEATABLE_READ)
	public void calculateBalanceOnRepeatableReadIsolationLevel(Long accountId, Long periodId) {
		calculateBalance(accountId, periodId);
	}

	@Transactional(isolation = Isolation.SERIALIZABLE)
	public void calculateBalanceOnSerializableIsolationLevel(Long accountId, Long periodId) {
		calculateBalance(accountId, periodId);
	}

	private void calculateBalance(Long accountId, Long periodId) {
		var totalAsSender = transferRepository.findAllBySenderIdAndPeriodIdOrderById(accountId, periodId)
				.map(Transfer::getAmount)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		var totalAsReceiver = transferRepository.findAllBySenderIdAndPeriodIdOrderById(accountId, periodId)
				.map(Transfer::getAmount)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		var balance = totalAsReceiver.subtract(totalAsSender);
		log.debug("Account [{}], period [{}], balance: {}", accountId, periodId, balance);
		try {
			Thread.sleep(10); // emulate long running computations
		} catch (InterruptedException e) {
			throw new InterruptedRuntimeException();
		}
	}
}
