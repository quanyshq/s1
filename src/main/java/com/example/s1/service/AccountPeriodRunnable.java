package com.example.s1.service;

public interface AccountPeriodRunnable {
	void run(Long accountId, Long periodId);
}
