package com.example.s1.service;

import com.example.s1.repository.TransferRepository;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
@Slf4j
public class CheckService {
	private final TransferRepository transferRepository;

	private void checkTransfers(Long accountId, Long periodId) {
		transferRepository.findAllBySenderIdAndPeriodIdOrderById(accountId, periodId)
				.forEach(t -> {
					t.setId(null);
					t.setCheckedAt(Instant.now());
					transferRepository.save(t);
				});
		transferRepository.deleteAll(transferRepository.findAllUncheckedSenderTransfers(accountId, periodId));
	}

	@Timed("_0_WARMUP")
	public void warmup(Long accountId, Long periodId) {
		log.info("Warmup checking");
		checkTransfers(accountId, periodId);
	}

	@Timed("_1_READ_UNCOMMITTED")
	public void checkForReadUncommittedIsolationLevel(Long accountId, Long periodId) {
		log.info("Checking on READ UNCOMMITTED isolation level");
		checkTransfers(accountId, periodId);
	}

	@Timed("_2_READ_COMMITTED")
	public void checkForReadCommittedIsolationLevel(Long accountId, Long periodId) {
		log.info("Checking on READ COMMITTED isolation level");
		checkTransfers(accountId, periodId);
	}

	@Timed("_3_REPEATABLE_READ")
	public void checkForRepeatableReadIsolationLevel(Long accountId, Long periodId) {
		log.info("Checking on REPEATABLE READ isolation level");
		checkTransfers(accountId, periodId);
	}

	@Timed("_4_SERIALIZABLE")
	public void checkForSerializableIsolationLevel(Long accountId, Long periodId) {
		log.info("Checking on SERIALIZABLE isolation level");
		checkTransfers(accountId, periodId);
	}
}
