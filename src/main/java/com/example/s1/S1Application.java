package com.example.s1;

import com.example.s1.exception.InterruptedRuntimeException;
import com.example.s1.service.AccountPeriodRunnable;
import com.example.s1.service.BalanceService;
import com.example.s1.service.CheckService;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import jakarta.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.Comparator.comparing;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class S1Application implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args).close();
	}

	private final MeterRegistry meterRegistry;
	private final CheckService checkService;
	private final BalanceService balanceService;
	private final AtomicLong accountIdSequence = new AtomicLong();
	private final AtomicLong periodIdSequence = new AtomicLong();

	@Override
	public void run(String... args) {
		checkAndCalculateBalanceInBackground(
				checkService::warmup,
				balanceService::warmup);
		checkAndCalculateBalanceInBackground(
				checkService::warmup,
				balanceService::warmup);
		checkAndCalculateBalanceInBackground(
				checkService::checkForReadUncommittedIsolationLevel,
				balanceService::calculateBalanceOnReadUncommittedIsolationLevel);
		checkAndCalculateBalanceInBackground(
				checkService::checkForReadCommittedIsolationLevel,
				balanceService::calculateBalanceOnReadCommittedIsolationLevel);
		checkAndCalculateBalanceInBackground(
				checkService::checkForRepeatableReadIsolationLevel,
				balanceService::calculateBalanceOnRepeatableReadIsolationLevel);
		checkAndCalculateBalanceInBackground(
				checkService::checkForSerializableIsolationLevel,
				balanceService::calculateBalanceOnSerializableIsolationLevel);
	}

	private void checkAndCalculateBalanceInBackground(AccountPeriodRunnable checkMethod, AccountPeriodRunnable calculateMethod) {
		var accountId = accountIdSequence.incrementAndGet();
		var periodId = periodIdSequence.incrementAndGet();
		var calculationThread = new Thread(() -> {
			try {
				log.info("Calculating balance: account={}, period={}", accountId, periodId);
				while (!Thread.interrupted()) {
					calculateMethod.run(accountId, periodId);
				}
			} catch (InterruptedRuntimeException e) {
				log.debug(e.getMessage(), e);
			}
		});
		calculationThread.start();
		checkMethod.run(accountId, periodId);
		try {
			calculationThread.interrupt();
			calculationThread.join();
		} catch (
				InterruptedException e) {
			log.warn(e.getMessage(), e);
		}
	}

	@PreDestroy
	public void logDurations() {
		meterRegistry.getMeters().stream()
				.filter(Timer.class::isInstance)
				.map(Timer.class::cast)
				.filter(timer -> timer.getId().getName().startsWith("_"))
				.sorted(comparing(timer -> timer.getId().getName()))
				.forEach(timer -> {
					var name = StringUtils.rightPad(timer.getId().getName(), 20);
					var duration = (long) timer.totalTime(TimeUnit.MILLISECONDS);
					log.info("[{}] meter: {} ms", name, duration);
				});
	}
}
