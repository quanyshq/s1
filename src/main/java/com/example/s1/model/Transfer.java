package com.example.s1.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@ToString
@Builder
public class Transfer {
	@Id
	private Long id;
	private Long senderId;
	private Long receiverId;
	private Long periodId;
	private BigDecimal amount;
	private Instant checkedAt;
}
