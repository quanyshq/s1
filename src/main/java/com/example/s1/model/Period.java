package com.example.s1.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Getter
@Setter
@ToString
public class Period {
	@Id
	private Long id;
	private LocalDate day;
}
