package com.example.s1.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@ToString
public class Account {
	@Id
	private Long id;
	private String customerName;
}
