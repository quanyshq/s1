package com.example.s1.repository;

import com.example.s1.model.Transfer;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, Long> {
	Stream<Transfer> findAllBySenderIdAndPeriodIdOrderById(Long senderId, Long periodId);

	@Query("select * from transfer where sender_id = :accountId and period_id = :periodId and checked_at is null")
	List<Transfer> findAllUncheckedSenderTransfers(Long accountId, Long periodId);
}
