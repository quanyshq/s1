package com.example.s1.repository;

import com.example.s1.model.Period;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeriodRepository extends CrudRepository<Period, Long> {
	List<Period> findAllByOrderById();
}
