package com.example.s1.repository;

import com.example.s1.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
	List<Account> findAllByOrderById();
}
