insert into numbers (id)
select row_number() over (order by object_id)
from sys.all_objects;

insert into customer(name, city)
values ('John', 'Astana'),
       ('Bob', 'Astana'),
       ('Alice', 'Almaty'),
       ('Carol', 'Astana'),
       ('Craig', 'Astana'),
       ('Ivan', 'Almaty'),
       ('Mike', 'Almaty'),
       ('Ted', 'Almaty'),
       ('Wendy', 'Almaty');

insert into account(customer_name)
select c.name
from customer as c
         cross join numbers as n
where n.id <= 10;

insert into period (day)
select dateadd(day, id, '2023-01-01')
from numbers
where id <= 10

insert into transfer(sender_id, receiver_id, period_id, amount)
select s.id, r.id, p.id, 100.0
from account as s
         cross join account as r
         cross join period as p;

