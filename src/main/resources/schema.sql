drop table if exists transfer;
drop table if exists period;
drop table if exists account;
drop table if exists customer;
drop table if exists numbers;

create table numbers
(
    id bigint primary key
);

create table customer
(
    name varchar(255) primary key,
    city varchar(255) not null
);

create table account
(
    id            bigint identity primary key,
    customer_name varchar(255) not null references customer (name)
);

create table period
(
    id  bigint identity primary key,
    day date not null
);

create table transfer
(
    id          bigint identity primary key,
    sender_id   bigint         not null references account (id),
    receiver_id bigint         not null references account (id),
    period_id   bigint         not null references period (id),
    amount      numeric(12, 2) not null,
    checked_at  datetime
);
